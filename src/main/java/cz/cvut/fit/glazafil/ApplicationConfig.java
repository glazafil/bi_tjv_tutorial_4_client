package cz.cvut.fit.glazafil;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("cz.cvut.fit.glazafil")
public class ApplicationConfig {}
