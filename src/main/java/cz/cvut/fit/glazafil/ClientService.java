package cz.cvut.fit.glazafil;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ClientService {

        private final RestTemplate restTemplate;

        public ClientService() {
            this.restTemplate = new RestTemplate();
        }

        public String getGreeting() {
            String url = "http://localhost:8080/greeting";
            return this.restTemplate.getForObject(url, String.class);
        }
}
